import socket
import random
import os

def encrypt_message(text, k):
	print text
	return str(map(lambda x: ord(x)^k, list(text)))[1:-1]

def decrypt_message(text, k):
	lst = text.split(',')
	lst = map(lambda x: chr(x^k),map(lambda x: int(x[:-1]), lst))
	return  ''.join(lst)

def calc(r, g, p):
	return (int(g) ** r) % p

def connect():
	s = socket.socket()
	s.bind(("0.0.0.0", 8888))
	s.listen(1)
	cs, ca = s.accept()
	handle_client(cs)
	cs.close()
	s.close()

def handle_client(cs):
	data = cs.recv(1024)
	g, p, his_random = map(lambda x: int(x), data.split(','))

	my_random = random.randint(3, 1000)
	my_calculated_random = calc(my_random, g, p)

	same_key = calc(my_random, his_random, p)

	cs.send(str(my_calculated_random))
	
	print "The calculated key: " + str(same_key)
	message = raw_input("Enter message: ")
	
	while True:
		cs.send(encrypt_message(message, same_key))
		
		print message + "- Sended!"
		data = cs.recv(1024)
		data = 	decrypt_message(data, same_key)
		print data + "- Got!"
		
		if data == "_q":
			cs.send(encrypt_message(data, same_key))
			break
		message = raw_input("Enter message: ")

connect()
os.system("pause")