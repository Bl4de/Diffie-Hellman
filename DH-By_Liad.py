import random
import socket
import os
IP = "127.0.0.1"
PORT = 8888

def is_prime(n):
	if n == 2 or n == 3: return True
	if n < 2 or n%2 == 0: return False
	if n < 9: return True
	if n%3 == 0: return False
	r = int(n**0.5)
	f = 5
	while f <= r:
		if n%f == 0: return False
		if n%(f+2) == 0: return False
		f +=6
	return True    

def generate_prime():
	minPrime = 0
	maxPrime = 1000
	cached_primes = [i for i in range(minPrime,maxPrime) if is_prime(i)]
	return random.choice(cached_primes)


def get_gp():
	g = generate_prime()
	p = g
	while p == g:
		p = generate_prime()
	return (g, p)


def calc(r, g, p):
	return (int(g) ** r) % p

def encrypt_message(text, k):
	print text
	return str(map(lambda x: ord(x)^k, list(text)))[1:-1]

def decrypt_message(text, k):
	lst = text.split(',')
	lst = map(lambda x: chr(x^k),map(lambda x: int(x[:-1]), lst))
	return  ''.join(lst)


def connect():
	g, p = get_gp()
	
	s = socket.socket()
	s.connect((IP, PORT))
	
	my_random = random.randint(3, 1000)
	my_calculated_random = calc(my_random, g, p)

	s.send(str(g) + "," + str(p) + "," + str(my_calculated_random))
	
	same_key = calc(my_random, int(s.recv(1024)), p)

	print "The calculated key: " + str(same_key)
	
	while True:
		data = decrypt_message(s.recv(1024), same_key)
		print data + "- Got!"
		if data == "_q":
			s.send(encrypt_message(data, same_key))
			break

		message = raw_input("Enter message: ")
		s.send(encrypt_message(message, same_key))
		print message + "- Sended!"
	s.close()
	os.system("pause")


connect()